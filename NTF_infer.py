import numpy as np
from numpy import log, exp
import tensorflow as tf
import tensorflow.contrib.layers as layers
from tensorflow.contrib.layers import xavier_initializer, batch_norm
from utils import *
from model import *
import argparse


class NTF(object):
    def __init__(self, pretrain_flag, save_flag,
                 batch_size, embedding_size, hidden_state, learning_rate, num_steps,
                 Tvocabulary_size, Mvocabulary_size, Uvocabulary_size, dictionaryT,
                 org_range_min, org_range_max, new_range_min, new_range_max):  # n is number of users

        self.pretrain_flag = pretrain_flag
        self.save_flag = save_flag
        self.batch_size = batch_size
        self.dictionaryT = dictionaryT
        self.num_steps = num_steps

        self.NN = NN(embedding_size=embedding_size,
                     hidden_state=hidden_state,
                     learning_rate=learning_rate,
                     num_steps=num_steps,
                     Tvocabulary_size=Tvocabulary_size,
                     Mvocabulary_size=Mvocabulary_size,
                     Uvocabulary_size=Uvocabulary_size,
                     org_range_min=org_range_min,
                     org_range_max=org_range_max,
                     new_range_min=new_range_min,
                     new_range_max=new_range_max)

        self.input_midx, self.input_uidx, self.input_tseq, self.obsv, \
        self.n_instances, self.pred, self.loss, self.pred_orgscale, \
        self.obsv_orgscale, self.input_embeddingT, self.train_op, \
        self.keep_rate, self.phase, self.phase_RNN, self.rmse, self.rsquare, self.mae, self.init = self.NN.model()

        self.avg_loss = 0
        self.avg_rmse = 0
        self.performance = 0
        self.performance = [5, 5, 5]
        self.max_performance = [5, 5, 5]

    def load_test_data(self, ts, us, ms, ys):
        self.tst_ts = ts
        self.tst_us = us
        self.tst_ms = ms
        self.tst_ys = ys

    def load_validation_data(self, ts, us, ms, ys):
        self.valid_ts = ts
        self.valid_us = us
        self.valid_ms = ms
        self.valid_ys = ys

    def create(self, pretrain_flag=0, save_file=''):
        config = tf.ConfigProto()
        config.gpu_options.allow_growth = True
        config.gpu_options.per_process_gpu_memory_fraction = 0.8
        config.log_device_placement = True
        config.allow_soft_placement = True
        tf.set_random_seed(123)
        self.sess = tf.Session(config=config)
        if pretrain_flag == 0:
            self.sess.run(self.init)
        else:
            tf.train.Saver().restore(self.sess, save_file + '.ckpt')
        print('create')

    def run_eval(self, local_ts, local_us, local_ms, local_ys, split_instance=10000):
        local_Tseqs = transfer_model_sequence(self.dictionaryT, local_ts, self.num_steps)
        pred_ys = []
        obsv_ys = []
        xs_batches = 1 + len(local_ts) // split_instance
        for idx in range(xs_batches):
            idx_begin = idx * split_instance
            idx_end = min((idx + 1) * split_instance, len(local_ts))
            feed_dict = {self.input_uidx: np.array(local_us[idx_begin:idx_end]).reshape(-1, 1), \
                         self.obsv: local_ys[idx_begin:idx_end], \
                         self.input_midx: np.array(local_ms[idx_begin:idx_end]).reshape(-1, 1), \
                         self.input_tseq: np.array(local_Tseqs[idx_begin:idx_end]), \
                         self.n_instances: len(local_ts[idx_begin:idx_end]), self.keep_rate: 1.0, self.phase: 0, \
                         self.phase_RNN: 0}
            pred_y, obsv_y = self.sess.run([self.pred_orgscale, self.obsv_orgscale], feed_dict=feed_dict)
            pred_ys = pred_ys + list(pred_y)
            obsv_ys = obsv_ys + list(obsv_y)
        my_rmse = RMSE(pred_ys, obsv_ys)
        my_rsquare = Rsquare(pred_ys, obsv_ys)
        my_mae = MAE(pred_ys, obsv_ys)
        print('obsv y:', obsv_ys[:10])
        print('pred y:', [round(float(_), 1) for _ in pred_ys[:10]])
        return (my_rmse, my_rsquare, my_mae)

    def updateParameters(self, epoch_idx, step, batch_ts, batch_us, batch_ms, batch_ys, eval=True, valid=False):
        batch_Tseqs = transfer_model_sequence(self.dictionaryT, batch_ts, self.num_steps)
        feed_dict1 = {self.input_uidx: np.array(batch_us).reshape(-1, 1), \
                      self.obsv: batch_ys, self.input_midx: np.array(batch_ms).reshape(-1, 1),
                      self.input_tseq: np.array(batch_Tseqs), \
                      self.n_instances: self.batch_size, self.keep_rate: 1.0, self.phase: 1, self.phase_RNN: 1}

        'calculate avg_loss'
        if self.save_flag == 1:
            _, loss_val, rmse_ = self.sess.run([self.train_op, self.loss, self.rmse], feed_dict=feed_dict1)
        else:
            loss_val, rmse_ = self.sess.run([self.loss, self.rmse], feed_dict=feed_dict1)
        self.avg_loss += loss_val
        self.avg_rmse += rmse_

        if step % 500 == 0:
            self.avg_loss /= 500
            self.avg_rmse /= 500
            print(
            '(epoch %s, step %s): avg_loss = %.4f avg_rmse = %.4f' % (epoch_idx, step, self.avg_loss, self.avg_rmse),
            end ='\r')
            self.avg_loss = 0
            self.avg_rmse = 0

        if step % 1000 == 0 and epoch_idx >= 1 and eval:
            if valid:
                my_performance = self.run_eval(self.valid_ts, self.valid_us, self.valid_ms, self.valid_ys)
                if my_performance[0] < self.max_performance[0]:
                    self.max_performance = my_performance
                    tst_performance = self.run_eval(self.tst_ts, self.tst_us, self.tst_ms, self.tst_ys)
                    self.performance = tst_performance
                    print('(epoch %s, step %s) validation performance: rmse = %.4f mae = %.4f' % (
                        epoch_idx, step, my_performance[0], my_performance[2]))
                    print('(epoch %s, step %s) test performance: rmse = %.4f mae = %.4f' % (
                        epoch_idx, step, tst_performance[0], tst_performance[2]))
                print('(epoch %s, step %s) best performance: rmse = %.4f mae = %.4f' % (
                    epoch_idx, step, self.performance[0], self.performance[2]))
                print('')
            else:
                my_performance = self.run_eval(self.tst_ts, self.tst_us, self.tst_ms, self.tst_ys)
                if my_performance[0] < self.max_performance[0]:
                    self.max_performance = my_performance
                    self.performance = self.max_performance
                print('current performance: rmse = %.4f mae = %.4f' % (my_performance[0], my_performance[2]))
                print('(epoch %s, step %s) best performance: rmse = %.4f mae = %.4f' % (
                    epoch_idx, step, self.performance[0], self.performance[2]))
                print('')


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--batch', type=int, default=128, help=u"number of epochs")
    parser.add_argument('--pretrain', type=bool, default=0, help=u"load pretrained paras")
    parser.add_argument('--save', type=bool, default=1, help=u"save trained paras? 0/1")
    parser.add_argument('--embed', type=int, default=32, help=u"dim of embeddings")
    parser.add_argument('--hidden', type=int, default=32, help=u"dim of LSTM hidden state")
    parser.add_argument('--steps', type=int, default=5, help=u"number of time steps")
    parser.add_argument('--lr', type=int, default=0.001, help=u"learning rate")
    args = parser.parse_args()

    batch_size = args.batch
    pretrain_flag = args.pretrain
    save_flag = args.save
    embedding_size = args.embed
    learning_rate = args.lr
    num_steps = args.steps
    hidden_state = args.hidden

    org_range_min = 1
    org_range_max = 5
    new_range_min = -1
    new_range_max = 1

    Tvocabulary_size = 100
    Mvocabulary_size = 1000
    Uvocabulary_size = 5000

    dictionaryT = {tid: idx + 1 for idx, tid in enumerate(range(Tvocabulary_size))}
    dictionaryT['Nan'] = 0

    alg = NTF(pretrain_flag, save_flag, batch_size, embedding_size,
              hidden_state, learning_rate, num_steps,
              Tvocabulary_size, Mvocabulary_size, Uvocabulary_size, dictionaryT,
              org_range_min, org_range_max, new_range_min, new_range_max)
    alg.create(pretrain_flag=0, save_file='')

    'generate & load validation data'
    valid_ys = np.random.randint(org_range_min, high=org_range_max, size=500)
    valid_ts = np.random.randint(0, high=Tvocabulary_size, size=500)
    valid_us = np.random.randint(0, high=Uvocabulary_size, size=500)
    valid_ms = np.random.randint(0, high=Mvocabulary_size, size=500)
    scaled_valid_ys_ys = scale(valid_ys, org_range_min, org_range_max, new_range_min, new_range_max)
    alg.load_validation_data(valid_ts, valid_us, valid_ms, scaled_valid_ys_ys)

    'generate & load testing data'
    tst_ys = np.random.randint(org_range_min, high=org_range_max, size=500)
    tst_ts = np.random.randint(0, high=Tvocabulary_size, size=500)
    tst_us = np.random.randint(0, high=Uvocabulary_size, size=500)
    tst_ms = np.random.randint(0, high=Mvocabulary_size, size=500)
    scaled_tst_ys_ys = scale(tst_ys, org_range_min, org_range_max, new_range_min, new_range_max)
    alg.load_test_data(tst_ts, tst_us, tst_ms, scaled_tst_ys_ys)

    train_size = 250000
    train_ys = np.random.randint(org_range_min, high=org_range_max, size=train_size)
    train_ts = np.random.randint(0, high=Tvocabulary_size, size=train_size)
    train_us = np.random.randint(0, high=Uvocabulary_size, size=train_size)
    train_ms = np.random.randint(0, high=Mvocabulary_size, size=train_size)
    scaled_train_ys = scale(train_ys, org_range_min, org_range_max, new_range_min, new_range_max)

    for epoch_idx, epoch in enumerate(gen_epochs(2, list(range(train_size)), batch_size)):
        for step, batch_idx in enumerate(epoch):
            batch_ts = train_ts[batch_idx]
            batch_us = train_us[batch_idx]
            batch_ms = train_ms[batch_idx]
            scaled_batch_ys = scaled_train_ys[batch_idx]
            alg.updateParameters(epoch_idx, step, batch_ts, batch_us, batch_ms, scaled_batch_ys, eval=True, valid=True)