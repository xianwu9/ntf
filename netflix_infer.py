import csv
import time
import datetime as DT
from matplotlib.dates import date2num, num2date
from matplotlib import pyplot as plt
import operator
import collections
from collections import OrderedDict
import copy
import random
import argparse
from sklearn.preprocessing import MinMaxScaler
import os
from multiprocessing import Pool
from multiprocessing.dummy import Pool as ThreadPool
from sklearn.model_selection import train_test_split
import numpy as np
from NTF_infer import *

########################################################
# load files
def load_data(file_name):
    csv_file_name = csv.reader(open(file_name, 'rU'), dialect='excel')
    header_file_name = next(csv_file_name)
    column_file_name = {}
    for h in header_file_name:
        column_file_name[h] = []
    for idx, row in enumerate(csv_file_name):
        for h, v in zip(header_file_name, row):
            column_file_name[h].append(v)
    return header_file_name, column_file_name

# load files
def load_txt_data(file_name, header):
    print(file_name)
    column_file_name = {};
    header_file_name = header
    for h in header_file_name:
        column_file_name[h] = []
    for idx, val in enumerate(open(file_name)):
        row = [_.strip('\n') for _ in val.split(',')][:-1]
        for h, v in zip(header_file_name, row):
            column_file_name[h].append(v)
    return header_file_name, column_file_name
#########################################################
def convert2idx(potential_words, idx, offset):
    words = [potential_words[_] for _ in idx]
    count = collections.Counter(words)
    dictionary = dict()
    for word in count:
        dictionary[word] = len(dictionary) + offset
    reverse_dictionary = dict(zip(dictionary.values(), dictionary.keys()))
    return dictionary, reverse_dictionary

def convert2dtidx(potential_words, idx, offset):
    words = [potential_words[_] for _ in idx]
    count = collections.Counter(words)
    date2vs = {date: Date2month(date) for date in count}
    min_date = min(list(date2vs.values()))
    dictionary = dict()
    for word in count:
        dictionary[word] = date2vs[word] + offset - min_date
    return dictionary
#########################################################
def split_idx(total_num, train_r, val_r, tst_r):
    labels = range(total_num)
    labels_train, labels_valt = train_test_split(labels, test_size=(val_r+tst_r)/float(val_r+tst_r+train_r), random_state=42)
    labels_val, labels_test = train_test_split(labels_valt, test_size=tst_r/float(val_r+tst_r), random_state=42)
    return(labels_train, labels_val, labels_test)

def transfer_model_index(indices):
    tmi_ts = [dictionaryT[columns['date'][idx]] for idx in indices]
    tmi_us = [dictionaryU[columns['userid'][idx]] for idx in indices]
    tmi_ms = [dictionaryM[columns['movieid'][idx]] for idx in indices]
    tmi_ys = [scale(columns['stars'][idx], org_range_min, org_range_max, new_range_min, new_range_max) for idx in indices]
    return (tmi_ts, tmi_us, tmi_ms, tmi_ys)
###############################################
DateFormat = lambda v: DT.datetime.fromordinal(int(v)).strftime("%Y-%m-%d")
Date2v = lambda date: date2num(DT.datetime.strptime(date, "%Y-%m-%d"))
Date2month = lambda date: int(date[:4]) * 12 + int(date[5:7])
###############################################
parser = argparse.ArgumentParser()
parser.add_argument('--batch', type=int, default=256 ,help=u"number of epochs")
parser.add_argument('--pretrain', type=bool, default=0 ,help=u"load pretrained paras")
parser.add_argument('--save', type=bool, default=1 ,help=u"save trained paras? 0/1")
parser.add_argument('--embed', type=int, default=32 ,help=u"dim of embeddings")
parser.add_argument('--hidden', type=int, default=32,help=u"dim of LSTM hidden state dim")
parser.add_argument('--steps', type=int, default=5,help=u"number of time steps")
parser.add_argument('--lr', type=int, default=0.001 ,help=u"learning rate")
args = parser.parse_args()

batch_size = args.batch;
pretrain_flag = args.pretrain;
save_flag = args.save;
embedding_size = args.embed;
learning_rate = args.lr;
num_steps = args.steps;
hidden_state = args.hidden;
'###############################################################'
train_r = 5; val_r= 1; tst_r = 10 - train_r - val_r
'###############################################################'
'load data'
input_file = 'dataset/netflix.csv'
header, columns = load_data(input_file)# header = ['movieid', 'userid', 'date', 'stars']
for h in columns:
    print(h, len(set(columns[h])), columns[h][:10])
    if h == 'stars':
        columns[h] = list(map(int, columns[h]))
total_num = len(columns[h])
train_idx,valid_idx,tst_idx = split_idx(total_num, train_r, val_r, tst_r)
total_num = len(train_idx) + len(tst_idx)

dictionaryU, reverse_dictionaryU = convert2idx(columns['userid'], train_idx + valid_idx, 0)
dictionaryM, reverse_dictionaryM = convert2idx(columns['movieid'], train_idx + valid_idx, 0)
dictionaryT = convert2dtidx(columns['date'], train_idx + valid_idx + tst_idx, 1)
dictionaryT['Nan'] = 0
'###############################################################'
org_range_min = min(columns['stars']); org_range_max = max(columns['stars']);
new_range_min = -1; new_range_max = 1;
'###############################################################'
(val_ts, val_us, val_ms, val_ys) = transfer_model_index(valid_idx)
(tst_ts, tst_us, tst_ms, tst_ys) = transfer_model_index(tst_idx)
'###############################################################'
Tvocabulary_size = len(list(set(dictionaryT.values()))) + 2
Mvocabulary_size = len(list(set(dictionaryM.values()))) + 2
Uvocabulary_size = len(list(set(dictionaryU.values()))) + 2
print('total_num:%s, U:%s, M:%s, T:%s' %(total_num, Uvocabulary_size, Mvocabulary_size, Tvocabulary_size))

alg = NTF(pretrain_flag, save_flag, batch_size, embedding_size,
          hidden_state, learning_rate, num_steps,
          Tvocabulary_size, Mvocabulary_size, Uvocabulary_size, dictionaryT,
          org_range_min, org_range_max, new_range_min, new_range_max)

alg.create(pretrain_flag=0, save_file='')
alg.load_validation_data(val_ts, val_us, val_ms, val_ys)
alg.load_test_data(tst_ts, tst_us, tst_ms, tst_ys)
for epoch_idx, epoch in enumerate(gen_epochs(20, train_idx, batch_size)):
    for step, batch_idx in enumerate(epoch):
        (batch_ts, batch_us, batch_ms, batch_ys) = transfer_model_index(batch_idx)
        batch_Tseqs = transfer_model_sequence(dictionaryT, batch_ts, num_steps)
        alg.updateParameters(epoch_idx, step, batch_ts, batch_us, batch_ms, batch_ys, eval=True, valid=True)
