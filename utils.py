import numpy as np

def rescale_linear(array, org_range, new_range):
    (org_range_min, org_range_max) = org_range
    (new_range_min, new_range_max) = new_range
    m = (new_range_max - new_range_min) / (org_range_max - org_range_min)
    b = new_range_min - m * org_range_min
    return m * array + b

scale = lambda x, org_range_min, org_range_max, new_range_min, new_range_max: rescale_linear(x, (org_range_min, org_range_max), (new_range_min, new_range_max))
scale_back = lambda x, org_range_min, org_range_max, new_range_min, new_range_max: rescale_linear(x, (new_range_min, new_range_max), (org_range_min, org_range_max))


def RMSE(estimation, truth):
    """Root Mean Square Error"""
    estimation = np.float64(estimation)
    truth = np.float64(truth)
    num_sample = estimation.shape[0]
    # sum square error
    sse = np.sum(np.square(truth - estimation))
    return np.sqrt(np.divide(sse, num_sample - 1))


def Rsquare(estimation, truth):
    """Root Mean Square Error"""
    estimation = np.float64(estimation)
    truth = np.float64(truth)
    num_sample = estimation.shape[0]
    # sum square error
    sse = np.mean(np.square(truth - estimation))
    var = np.mean(np.square(truth - np.mean(truth)))
    return 1 - sse / var


def MAE(estimation, truth):
    """Root Mean Square Error"""
    estimation = np.float64(estimation)
    truth = np.float64(truth)
    num_sample = estimation.shape[0]
    sse = np.mean(np.abs(truth - estimation))
    return sse

def transfer_model_sequence(dictionaryT, ts, num_steps):
    tmp_tseqs = []
    for t in ts:
        if t < num_steps:
            tmp_tseq = [dictionaryT['Nan']] * (num_steps - t) + list(range(t))
        else:
            tmp_tseq = list(range(t - num_steps, t))
        tmp_tseqs.append(tmp_tseq)
    return tmp_tseqs

def pred_transfer_model_sequence(dictionaryT, ts, num_steps):
    tmp_tseqs = []
    for t in ts:
        if t < num_steps:
            tmp_tseq = [dictionaryT['Nan']] * (num_steps - t) + list(range(t))
        else:
            tmp_tseq = list(range(t - num_steps, t))
        tmp_tseq[-1] = dictionaryT['untrain']
        tmp_tseqs.append(tmp_tseq)
    return tmp_tseqs

def gen_batchs(train_idx, batch_size):
    rng_state = np.random.get_state()
    np.random.shuffle(train_idx)
    xs_batches = len(train_idx) // batch_size
    for idx in range(xs_batches):
        idx_begin = idx * batch_size
        idx_end = (idx + 1) * batch_size
        yield (train_idx[idx_begin:idx_end])

def gen_epochs(n, train_idx, batch_size):
    for i in range(n):
        yield (gen_batchs(train_idx, batch_size))