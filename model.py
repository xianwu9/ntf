import tensorflow as tf
import tensorflow.contrib.layers as layers
from tensorflow.contrib.layers import xavier_initializer, batch_norm
from utils import *

class NN(object):
    def __init__(self, **kwargs):
        self.embedding_size = kwargs['embedding_size']
        self.hidden_state = kwargs['hidden_state']
        self.learning_rate = kwargs['learning_rate']
        self.num_steps = kwargs['num_steps']

        self.Tvocabulary_size = kwargs['Tvocabulary_size']
        self.Mvocabulary_size = kwargs['Mvocabulary_size']
        self.Uvocabulary_size = kwargs['Uvocabulary_size']

        self.org_range_min = kwargs['org_range_min']
        self.org_range_max = kwargs['org_range_max']
        self.new_range_min = kwargs['new_range_min']
        self.new_range_max = kwargs['new_range_max']

    def MLP(self, inputs, input_dim, output_dim, str_name):
        with tf.variable_scope('mlp' + str_name):
            W_mlp = tf.get_variable('W_mlp', [input_dim, output_dim],
                                    initializer=tf.truncated_normal_initializer(stddev=0.1))
            b_mlp = tf.get_variable('b_mlp', [output_dim], initializer=tf.constant_initializer(0.0))
            output = tf.matmul(inputs, W_mlp) + b_mlp
        return output

    def my_LSTM(self, x1, embeddings, n_instances, keep_rate, phase_RNN):

        zero_state1 = tf.get_variable('zero_state1', [1, self.hidden_state], initializer=tf.constant_initializer(0.0))
        init_state1 = tf.matmul(tf.ones([n_instances, 1]), zero_state1)
        zero_state2 = tf.get_variable('zero_state2', [1, self.hidden_state], initializer=tf.constant_initializer(0.0))
        init_state2 = tf.matmul(tf.ones([n_instances, 1]), zero_state2)

        x_embeds = tf.nn.embedding_lookup(embeddings, x1)
        rnn1_inputs = tf.unstack(x_embeds, axis=1)  # unstack to num_steps of tensors

        'Definition of rnn_cell'
        WS = tf.get_variable('WS', [self.embedding_size + self.hidden_state, self.hidden_state],
                             initializer=tf.truncated_normal_initializer(stddev=0.1))
        bS = tf.get_variable('bS', [self.hidden_state], initializer=tf.constant_initializer(0.0))
        WI = tf.get_variable('WI', [self.embedding_size + self.hidden_state, self.hidden_state],
                             initializer=tf.truncated_normal_initializer(stddev=0.1))
        bI = tf.get_variable('bI', [self.hidden_state], initializer=tf.constant_initializer(0.0))
        WO = tf.get_variable('WO', [self.embedding_size + self.hidden_state, self.hidden_state],
                             initializer=tf.truncated_normal_initializer(stddev=0.1))
        bO = tf.get_variable('bO', [self.hidden_state], initializer=tf.constant_initializer(0.0))
        WF = tf.get_variable('WF', [self.embedding_size + self.hidden_state, self.hidden_state],
                             initializer=tf.truncated_normal_initializer(stddev=0.1))
        bF = tf.get_variable('bF', [self.hidden_state], initializer=tf.constant_initializer(0.0))

        def lstm_cell(rnn1_input, statec, stateh):
            input_t = tf.concat([rnn1_input, stateh], 1)
            igate = tf.sigmoid(tf.matmul(input_t, WI) + bI)
            ogate = tf.sigmoid(tf.matmul(input_t, WO) + bO)
            fgate = tf.sigmoid(tf.matmul(input_t, WF) + bF)
            statec_ = tf.tanh(
                tf.contrib.layers.batch_norm(tf.matmul(input_t, WS) + bS, scale=True, is_training=phase_RNN))
            statec = tf.multiply(fgate, statec) + tf.multiply(igate, tf.nn.dropout(statec_, keep_rate))
            stateh = tf.multiply(ogate, tf.tanh(statec))
            return statec, stateh

        statec = init_state1
        stateh = init_state2
        rnn_outputs = []
        for rnn1_input in rnn1_inputs:
            statec, stateh = lstm_cell(rnn1_input, statec, stateh)
            rnn_outputs.append(stateh)
        final_state = rnn_outputs[-1]
        return final_state

    def model(self, lr_decay=False):
        input_midx = tf.placeholder(tf.int32, shape=[None, 1])
        input_uidx = tf.placeholder(tf.int32, shape=[None, 1])
        input_tseq = tf.placeholder(tf.int32, shape=[None, self.num_steps])
        global_step = tf.Variable(0, trainable=False, name='global_step')
        obsv = tf.placeholder(tf.float32, [None, ])
        n_instances = tf.placeholder(tf.int32)
        keep_rate = tf.placeholder(tf.float32)
        phase = tf.placeholder(tf.bool, name='phase')
        phase_RNN = tf.placeholder(tf.bool, name='phase_RNN')

        '########'
        Uembeddings = tf.Variable(tf.random_uniform([self.Uvocabulary_size, self.embedding_size], -0.5, 0.5),
                                  dtype=tf.float32,
                                  name='Uembeddings')
        Membeddings = tf.Variable(tf.random_uniform([self.Mvocabulary_size, self.embedding_size], -0.5, 0.5),
                                  dtype=tf.float32,
                                  name='Membeddings')
        Tembeddings = tf.Variable(tf.random_uniform([self.Tvocabulary_size, self.embedding_size], -0.5, 0.5), dtype=tf.float32,
                                  name='Tembeddings')
        '########'
        input_embeddingU = tf.squeeze(tf.nn.embedding_lookup(Uembeddings, input_uidx), 1)
        input_embeddingM = tf.squeeze(tf.nn.embedding_lookup(Membeddings, input_midx), 1)
        embedt_ = self.my_LSTM(input_tseq, Tembeddings, n_instances, keep_rate, phase_RNN)
        Wt = tf.Variable(tf.random_normal([self.hidden_state, self.embedding_size], stddev=0.35), name='Wt')
        bt = tf.Variable(tf.zeros([self.embedding_size]), name='bt')
        lt11 = tf.contrib.layers.batch_norm(tf.add(tf.matmul(embedt_, Wt), bt), scale=True, is_training=phase)
        input_embeddingT = tf.sigmoid(lt11)

        input_vectors = tf.concat([input_embeddingU, input_embeddingM, input_embeddingT], axis=1)
        inputs = input_vectors
        input_dim = 3 * self.embedding_size
        output_dim = self.embedding_size
        for i in range(5):
            outputs = self.MLP(inputs, input_dim, output_dim, str(i))
            inputs = tf.nn.relu(outputs)
            input_dim = output_dim
        outputA = self.MLP(inputs, input_dim, self.embedding_size, 'A')
        inputs = self.MLP(outputA, self.embedding_size, input_dim, 'B')
        pred = tf.reduce_sum(self.MLP(inputs, input_dim, 1, 'prd'), axis=1)

        loss = tf.reduce_mean(tf.squared_difference(obsv, pred))

        if lr_decay:
            opt_func = tf.train.AdamOptimizer(
                learning_rate=tf.train.exponential_decay(self.learning_rate, global_step, decay_steps=5000,
                                                         decay_rate=0.99,
                                                         staircase=True))
        else:
            opt_func = tf.train.AdamOptimizer(learning_rate=self.learning_rate)
        tvars = tf.trainable_variables()
        grads, _ = tf.clip_by_global_norm(tf.gradients(loss, tvars), 1)
        train_op = opt_func.apply_gradients(zip(grads, tvars), global_step=global_step)

        pred_orgscale = scale_back(pred, self.org_range_min, self.org_range_max, self.new_range_min, self.new_range_max)
        obsv_orgscale = scale_back(obsv, self.org_range_min, self.org_range_max, self.new_range_min, self.new_range_max)

        yy_ = tf.squared_difference(pred_orgscale, obsv_orgscale)

        rmse = tf.sqrt(tf.reduce_mean(yy_))
        rsquare = 1 - tf.reduce_sum(yy_) / tf.reduce_sum(
            tf.squared_difference(obsv_orgscale, tf.reduce_mean(obsv_orgscale)))
        mae = tf.reduce_mean(tf.abs(pred_orgscale - obsv_orgscale))

        init = tf.global_variables_initializer()

        return input_midx, input_uidx, input_tseq, obsv, n_instances, \
               pred, loss, pred_orgscale, obsv_orgscale, input_embeddingT, \
               train_op, keep_rate, phase, phase_RNN, rmse, rsquare, mae, init